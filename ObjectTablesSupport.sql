USE DBA

CREATE TABLE LongRunningQueriesException
(
ID INT IDENTITY(1,1) NOT NULL,
[statement_text] nvarchar(128) NOT NULL,
CONSTRAINT [PK_LongRunningQueriesException] PRIMARY KEY CLUSTERED
([ID] ASC)
)

--TRUNCATE TABLE LongRunningQueriesException
--INSERT INTO LongRunningQueriesException VALUES ('%WAITFOR%')
--SELECT * FROM LongRunningQueriesException

CREATE TABLE LongRunningProgramNameException
(
ID INT IDENTITY(1,1) NOT NULL,
[program_name] nvarchar(128) NOT NULL,
CONSTRAINT [PK_LongRunningProgramNameException] PRIMARY KEY CLUSTERED
([ID] ASC)
)

--TRUNCATE TABLE LongRunningProgramNameException
--INSERT INTO LongRunningProgramNameException VALUES ('DatabaseMail%'), ('SSIS-Package%'), ('SQLAgent%'), ('SQL Management%'), ('Microsoft SQL Server Management%');
--SELECT * FROM LongRunningProgramNameException

CREATE TABLE LongRunningProcedureException
(
ID INT IDENTITY(1,1) NOT NULL,
[procedure_name] nvarchar(128) NOT NULL,
CONSTRAINT [PK_LongRunningProcedureException] PRIMARY KEY CLUSTERED
([ID] ASC)
)

--TRUNCATE TABLE LongRunningProcedureException
--INSERT INTO LongRunningProcedureException VALUES ('SomeProc');
--SELECT * FROM LongRunningProcedureException