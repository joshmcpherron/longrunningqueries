USE DBA

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SomeProc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SomeProc]
GO

CREATE PROC [SomeProc]      
AS                 
BEGIN  
	WAITFOR DELAY '02:00'

END

--EXEC SomeProc


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SomeProcs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [SomeProcs]
GO

CREATE PROC [SomeProcs]      
AS                 
BEGIN  
	WAITFOR DELAY '02:00'

END

--EXEC SomeProcs