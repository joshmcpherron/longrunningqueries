SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Josh McPherron
-- Create date: 08/28/2013
-- Description:	This process is to alert on long running queries.
-- =============================================    
USE [DBA]
GO

/****** Object:  StoredProcedure [DBA].[AlertLongRunningQueries]    Script Date: 08/28/2013 16:21:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[DBA].[AlertLongRunningQueries]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DBA].[AlertLongRunningQueries]
GO

USE [DBA]
GO

/****** Object:  StoredProcedure [DBA].[AlertLongRunningQueries]    Script Date: 08/28/2013 16:21:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Josh McPherron
-- Create date: 08/28/2013
-- Description:	This process is to alert on long running queries.
-- =============================================    
CREATE PROC [AlertLongRunningQueries]      
AS                 
BEGIN      
                
DECLARE @xml NVARCHAR(MAX),                
   @body NVARCHAR(MAX),
   @subjecttext NVARCHAR(100)              
                
 CREATE TABLE #Temp                 
 (                 
   [DATA]  NVARCHAR(MAX), 
   [SQLText] NVARCHAR(101)           
 )                
                        
INSERT INTO #Temp         
SELECT 'Database: ' + A.DatabaseName + '|' + 
		'SessionID: ' + cast(SessionID as nvarchar(10)) + '|' +  
		'Login: ' + Login_name + '|' + 
		'Status: ' + status + '|' + 
		'Wait Type: ' + a.wait_type + '|' + 
		'Blocking Session: ' + Cast(BlockingWith as NVARCHAR(10)) + '|' + 
		'Program Name: ' + A.[PROGRAM_NAME] + '|' +  
		'Seconds Running: ' + cast(A.RunningSeconds as NVARCHAR(10)) + '|' + 
		'Procedure: ' + COALESCE( Cast(A.ObjectName AS NVARCHAR(30)) , '' ) AS [DATA], 
		CAST(LEFT(a.statement_text, 100) AS NVARCHAR(100)) AS [SQLText],
		 LPNE.[program_name],
		 LPE.[procedure_name],
		 LQE.[statement_text],
		 a.RunningSeconds
  FROM activesessions A
	LEFT JOIN LongRunningProgramNameException LPNE
		ON A.[program_name] like LPNE.[program_name]
	LEFT JOIN LongRunningProcedureException LPE
		ON A.ObjectName = LPE.[procedure_name]
	LEFT JOIN LongRunningQueriesException LQE
		ON A.statement_text like LQE.[statement_text]
 WHERE A.RunningSeconds  > 10 AND (COALESCE(LPNE.[program_name], LPE.[procedure_name], LQE.[statement_text]) is null )
                
 SET @xml = CAST(( SELECT [DATA] AS 'td', [SQLText] AS 'td'             
 FROM  #Temp                
 FOR XML RAW('tr'), ELEMENTS ) AS NVARCHAR(MAX))                
                
 SET @body ='<html><body>'                
 SET @body =@body + '<table border = 1>                 
 <tr>                
 <th align="left"> Query Details </th>   
  <th align="left"> SQL Text </th>                  
 </tr>'                  
                
 IF (SELECT COUNT(DATA) FROM #Temp) = 0                
	BEGIN                
		RETURN              
	END                
	ELSE                
		SET @body = @body + @xml +'</table><BR>'                               
                
SET @body = @body + '</body></html>'    
SET @body = Replace(@body,'|','<BR>')                       
  
SET @subjecttext = @@servername + ' Alert: Long Running Query Detected'

PRINT @subjecttext
PRINT @body     
                
 --EXEC msdb.dbo.sp_send_dbmail                
 --@body = @body,                
 --@body_format ='HTML',                
 --@recipients = 'dba@allegiance.com', -- replace with your email address                
 ----@recipients = 'josh.mcpherron@allegiance.com', -- replace with your email address                
 --@subject = @subjecttext;

 END 