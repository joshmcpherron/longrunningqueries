use DBA

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ActiveSessions]'))
DROP VIEW [dbo].[ActiveSessions]
GO

CREATE VIEW ActiveSessions
AS
SELECT
	COALESCE(OBJECT_NAME(objectid, req.database_id),'Ad-Hoc') as ObjectName
 	, stateText.objectid  
	,SUBSTRING(stateText.text, (statement_start_offset/2)+1
	,((	CASE statement_end_offset
			WHEN -1 THEN DATALENGTH(stateText.text)
			ELSE statement_end_offset
			END - statement_start_offset)/2) + 1) AS statement_text
	,DB_Name(req.database_id) as DatabaseName
	,req.cpu_time AS CPU_Time
	,sess.last_request_start_time
	,datediff(second, CASE last_request_start_time
				WHEN '1900-01-01 00:00:00.000' THEN GETDATE()
				ELSE sess.last_request_start_time
				END
				,GETDATE()
			  ) AS RunningSeconds
	,COALESCE(req.wait_type,'') as wait_type
	,req.Percent_Complete
	,sess.[HOST_NAME] as RunningFrom
	,LEFT(CLIENT_INTERFACE_NAME, 25) AS RunningBy
	,sess.session_id AS SessionID
	,req.blocking_session_id AS BlockingWith
	,req.reads
	,req.writes
	,sess.[program_name]
	,sess.login_name
	,req.status
	,req.logical_reads
FROM sys.dm_exec_requests req
INNER JOIN sys.dm_exec_sessions sess ON sess.session_id = req.session_id
	AND sess.is_user_process = 1
CROSS APPLY sys.dm_exec_sql_text(req.sql_handle) AS stateText